# Readme
This script issues a HTTP/1.0 request and then echos back the Location header from the server's HTTP response.

It was found that on some systems, this will lead to disclosure of internal or external IP rather than the host name.

This script currently looks for response within:

* Location
* Content-Location
* WWW-Authenticate: Basic realm

Example:
![](ip_from_http_1_0.jpg)

## Usage

Using this script is really basic: `./one_oh.sh https://example.com`
