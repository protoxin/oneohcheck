#!/bin/bash

#There is probably a better way to do this. 
echo "######################################################"
echo "# HTTP/1.1 and HTTP/1.0 Internal IP Disclosure Check #"
echo "######################################################"
echo -ne "\n"
echo -ne "Target is: $1\n"
echo "[+]Sending requests..."

#HTTP/1.1 Checks
echo -ne "\nPerforming HTTP/1.1 Requests....\n"
echo -ne "\n[+]Location Header: "
curl -I -sS -k $1 -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:54.0) Gecko/20100101 Firefox/54.0" | tr -d '\r' | sed -En 's/^Location: (.*)/\1/p'
echo -ne "\n[+]Content-Location Header: "
curl -I -sS -k $1 -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:54.0) Gecko/20100101 Firefox/54.0" | tr -d '\r' | sed -En 's/^Content-Location: (.*)/\1/p'
echo -ne "\n[+]WWW-Authenticate: Basic realm Header: "
curl -I -sS -k $1 -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:54.0) Gecko/20100101 Firefox/54.0" | grep "WWW-Authenticate: Basic realm=" | awk -F'"' '{ print $2 }'
echo -ne "\n----------------------------------\n"
# HTTP/1.0 Checks
echo -ne "\nPerforming HTTP/1.0 Requests....\n"
echo -ne "\n[+]Location Header: "
curl -I -sS -k $1 --http1.0 --header 'Accept:' --header 'Connection:' --header  'Host:' -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:54.0) Gecko/20100101 Firefox/54.0" | tr -d '\r' | sed -En 's/^Location: (.*)/\1/p'
echo -ne "\n[+]Content-Location Header: "
curl -I -sS -k $1 --http1.0 --header 'Accept:' --header 'Connection:' --header  'Host:' -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:54.0) Gecko/20100101 Firefox/54.0" | tr -d '\r' | sed -En 's/^Content-Location: (.*)/\1/p'
echo -ne "\n[+]WWW-Authenticate: Basic realm Header: "
curl -I -sS -k $1 --http1.0 --header 'Accept:' --header 'Connection:' --header  'Host:' -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:54.0) Gecko/20100101 Firefox/54.0" | grep "WWW-Authenticate: Basic realm=" | awk -F'"' '{ print $2 }'
echo